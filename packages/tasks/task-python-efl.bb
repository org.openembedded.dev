DESCRIPTION = "Python Bindings to the Enlightenment Foundation Libraries"
LICENSE = "MIT"
SECTION = "devel/python"
RDEPENDS = "python-evas python-ecore python-emotion python-edje python-epsilon python-edbus"
PR = "ml2"

ALLOW_EMPTY = "1"
