DESCRIPTION = "Exml is a generic XML parser wrapper."
LICENSE = "MIT"
DEPENDS = "libxml2 libxslt ecore"
PV = "0.1.1+cvs${SRCDATE}"
PR = "r0"

inherit efl

