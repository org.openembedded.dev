DESCRIPTION = "Epsilon is a flexible and powerful image thumbnailing library \
that is compliant with the freedesktop.org Thumbnail Managing Standard."
LICENSE = "MIT BSD"
# can also depend on xine for movie thumbnails
DEPENDS = "imlib2 epeg libpng evas ecore edje perl-native"
PV = "0.3.0.012+cvs${SRCDATE}"
PR = "r0"

inherit efl

SRC_URI += "file://fix_alignment_error.patch;patch=1"

# a gstreamer thumbnailer would be nice now that we have emotion using gstreamer as well
EXTRA_OECONF = "--disable-xine"

FILES_${PN}-dev += "${libdir}/${PN}/*/*.a ${libdir}/${PN}/*/*.la"
FILES_${PN}-dbg += "${libdir}/${PN}/plugins/.debug/*.so"
