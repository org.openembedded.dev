DESCRIPTION = "EET is the Enlightenment data storage library"
DEPENDS = "zlib jpeg"
LICENSE = "MIT BSD"
PV = "0.9.10.042+cvs${SRCDATE}"
PR = "r0"

inherit efl
