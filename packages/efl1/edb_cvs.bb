DESCRIPTION = "Edb is the Enlightenment database library"
LICENSE = "MIT BSD"
DEPENDS = "zlib"
PV = "1.0.5.042+cvs${SRCDATE}"
PR = "r0"

inherit efl
